const Fs = require('fs');
const Path = require('path');
const Process = require('child_process');
const Logger = require('./logger');

class DependencyInstaller {
	async install(rootPath, type) {
		if (this[type]) await this[type](rootPath);

		return true;
	}

	nodejs(rootPath) {
		return new Promise((resolve, reject) => {
			const logPath = Path.join(rootPath, 'npm_install.log');
			const log = Fs.openSync(logPath, 'w');

			Logger.debug('Executing npm install...');
			const npmInstall = Process.spawn('npm', ['install', '--only=production'], {
				cwd: rootPath,
				stdio: ['ignore', log, log], // stdin, stdout, stderr
			});

			npmInstall.on('close', code => {
				Fs.closeSync(log);

				if (code !== 0) {
					return reject(new Error(`npm install exited with code ${code}`));
				}

				return resolve();
			});
		});
	}
}

module.exports = new DependencyInstaller();