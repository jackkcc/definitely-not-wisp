const Fs = require('fs');
const Path = require('path');
const Config = require('./config.js');

class Logger {
	constructor() {
		this.logPath = Config.get('LOG_PATH', Path.join(process.cwd(), 'logs', 'bootstrapper.log'));
		this.logLines = Config.get('LOG_LINES', 300);

		this.trimLog();
		this.writeCount = 0;
		this.logFile = Fs.createWriteStream(this.logPath, {flags: 'a'});
	}

	trimLog() {
		if (!Fs.existsSync(this.logPath)) {
			const dir = this.logPath.split('/');
			dir.pop();
			Fs.mkdirSync(dir.join('/'));
			return;
		}

		const logs = Fs.readFileSync(this.logPath).toString();
		const split = logs.split("\n");
		Fs.writeFileSync(this.logPath, split.slice(split.length - this.logLines).join("\n"));
	}

	checkLogs() {
		if (this.writeCount > this.logLines + 50) {
			this.trimLog();
			this.writeCount = 0;
		}
	}

	_write(message) {
		console.log(message);

		this.writeCount++;
		this.logFile.write(message + "\n");
		this.checkLogs();
	}

	timestamp() {
		return new Date().toISOString();
	}

	info(message) {
		this._write(`${this.timestamp()} [BOOTSTRAPPER] [INFO] ${message}`);
	}

	asProcess(process, message, stderr) {
		console.log(`[${process.toUpperCase()}]${stderr ? ' [ERR]' : ''} ${message}`);
	}

	warn(message) {
		this._write(`${this.timestamp()} [BOOTSTRAPPER] [WARN] ${message}`);
	}

	error(message) {
		this._write(`${this.timestamp()} [BOOTSTRAPPER] [ERROR] ${message}`);
	}

	fatal(message) {
		this._write(`${this.timestamp()} [BOOTSTRAPPER] [FATAL] ${message}`);
		process.exit(1);
	}

	debug(message) {
		const newMessage = `${this.timestamp()} [BOOTSTRAPPER] [DEBUG] ${message}`;

		this.writeCount++;
		this.logFile.write(newMessage + "\n");
		this.checkLogs();

		if (!Config.get('DEBUG', false)) return;
		console.log(newMessage);
	}
}

module.exports = new Logger();