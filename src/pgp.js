const Fs = require('fs');
const pgp = require('openpgp');
const Config = require('./config');

class PGP {
	constructor() {
		this.raw_pubkey = Fs.readFileSync(Config.get('GPG_PATH', 'wisp_gpg.asc'));
	}

	async getPublicKeys() {
		if (this.pubkeys) return this.pubkeys;

		return this.pubkeys = (await pgp.key.readArmored(this.raw_pubkey)).keys;
	}

	async getSigner(keyid) {
		const pubkeys = await this.getPublicKeys();

		for(const pubkey of pubkeys) {
			if (pubkey.getKeyIds().filter(a => a.equals(keyid)).length > 0) {
				return pubkey.getUserIds().join(', ');
			}
		}

		throw new Error('Unknown signer with the key id ' + keyid.toHex().toUpperCase());
	}

	verifyFile(path, signature) {
		return new Promise(async (resolve, reject) => {
			const pubkeys = await this.getPublicKeys();
			const pgpSignature = await pgp.signature.readArmored(signature);

			const readableStream = Fs.createReadStream(path);
			const message = pgp.message.fromBinary(readableStream);
			const verifyData = await pgp.verify({
				message: message,
				signature: pgpSignature,
				publicKeys: pubkeys,
			});

			await pgp.stream.readToEnd(verifyData.data); // https://github.com/openpgpjs/openpgpjs/issues/916
			const fileSignature = verifyData.signatures[0];
			fileSignature.verified = await fileSignature.verified;

			if (fileSignature.verified) fileSignature.signedBy = await this.getSigner(fileSignature.keyid);

			return resolve(fileSignature);
		});
	}
}

module.exports = new PGP();