const Fs = require('fs');
const Path = require('path');
const Logger = require('./logger');
const Config = require('./config');
const Updater = require('./updater');
const VersionChecker = require('./version-checker');
const ProcessManager = require('./process-manager');
const VersionResolver = require('./version-resolver');
const DependencyInstaller = require('./dependency-installer');

let projects;
try {
	projects = require(Config.get('PROJECTS_JSON_PATH', '../projects.json'));
} catch(err) {
	Logger.fatal(`Failed loading projects.json: ${err}`);
}

Logger.info('Hello world!');

async function handleBootstrapperUpdate() {
	Logger.info('Updating bootstrapper, waiting for all processes to shutdown...');
	await ProcessManager.safeShutdownAll();

	Logger.info('Exiting with code 0 to force image update...');
	process.exit(0);
}
Updater.addCustomHandler('bootstrapper', handleBootstrapperUpdate);
Updater.on('update-version', (projectName, version) => VersionChecker.setProject(projectName, version)); // Fixes circulate dependency requirement between updater and version checker.

async function init() {
	Logger.debug('Registering projects...');
	VersionChecker.setProject('bootstrapper', require('../package.json').version, process.env.BOOTSTRAPPER_CHANNEL || 'local');

	for(const project in projects) {
		const projectData = projects[project];
		const projectPath = Path.join(process.cwd(), project);
		const type = projectData.type;
		if (!Fs.existsSync(projectPath)) {
			Fs.mkdirSync(projectPath);
		}

		if (!Fs.existsSync(Path.join(projectPath, projectData.existency))) {
			Logger.info(`Project ${project} is missing, forcing it to update...`);
			VersionChecker.setProject(project, 'v0.0.0', projectData.channel);
		} else {
			VersionChecker.setProject(project, VersionResolver.get(projectPath, type), projectData.channel);
		}

		ProcessManager.registerProject(project, projectData);
	}

	Logger.debug('Running VersionChecker...');
	VersionChecker.enableAutorun();
	VersionChecker.run();
}

const triedDependencies = [];
function runProject(project, projectData) {
	return new Promise((resolve, reject) => {
		if (ProcessManager.isRunning(project)) return;

		Logger.info(`Running project ${project}...`);

		const projectPath = Path.join(process.cwd(), project);
		if (!Fs.existsSync(Path.join(projectPath, projectData.existency))) {
			if (projectData.fatal) Logger.fatal(`Project files for ${project} are missing, unable to run.`);
			else Logger.warn(`Project files for ${project} are missing but not marked as fatal, not running unless updated.`);
			return reject();
		}

		ProcessManager.start(project).then(() => {
			Logger.info(`Project ${project} is now running.`);
			return resolve();
		}).catch(err => {
			if (triedDependencies.includes(project)) {
				if (projectData.fatal) Logger.fatal(`Failed starting the project ${project}: ${err}`);
				else Logger.warn(`Failed starting the project ${project}, not running unless updated: ${err}`);
			} else Logger.warn(`Failed starting the project ${project}: ${err}`);

			Logger.info(`Attempting to automatically recover ${project}...`);
			triedDependencies.push(project);

			const dependencyFolder = Updater.resolveDependencyFolder(projectData.type);
			if (dependencyFolder) {
				if (!Fs.existsSync(Path.join(projectPath, dependencyFolder))) {
					DependencyInstaller.install(projectPath, projectData.type).then(() => {
						Logger.info(`Installed dependencies for ${project}, trying to run it again...`);

						runProject(project, projectData).then(resolve).catch(reject);
					}).catch(err => {
						Logger.fatal(`Failed to recover, dependency install for ${project} failed: ${err}`);
						return reject();
					});
					return;
				}
			}

			Logger[projectData.fatal ? 'fatal' : 'warn'](`Unable to automatically recover from current state for ${project}.`);
			return reject();
		});
	});
}

async function runProjects() {
	for(const project in projects) {
		await runProject(project, projects[project]).catch(() => {});
	}
}

VersionChecker.once('poll', updateable => {
	if (updateable > 0) {
		Logger.info('Delaying startup until everything is updated...');

		Updater.once('updated', runProjects);
	} else {
		runProjects();
	}
});

init().catch(err => {
	Logger.fatal('Failed initializing bootstrapper: ' + err);
});