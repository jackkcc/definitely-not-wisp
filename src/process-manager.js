const Path = require('path');
const Process = require('child_process');
const Kill = require('tree-kill');
const Logger = require('./logger');

class ProcessManager {
	constructor() {
		this.cwd = process.cwd();
		this.projectData = {};
		this.processes = {};
		this.crashes = {};
	}

	registerProject(project, projectData) {
		this.projectData[project] = projectData;
	}

	isRunning(projectName) {
		return this.processes[projectName] ? true : false;
	}

	addCrash(projectName) {
		if (!this.crashes[projectName]) this.crashes[projectName] = 0;

		return this.crashes[projectName]++;
	}

	clearCrashes(projectName) {
		delete this.crashes[projectName];
	}

	getCrashes(projectName) {
		return this.crashes[projectName] ? this.crashes[projectName] : 0;
	}
	
	async safeShutdown(projectName) {
		if (!this.processes[projectName]) return;

		const process = this.processes[projectName];
		const result = Kill(process.pid);

		delete this.processes[projectName];

		return result;
	}

	async safeShutdownAll() {
		const promises = [];
		for (const projectName in this.processes) {
			promises.push(this.safeShutdown(projectName));
		}

		return await Promise.all(promises);
	}

	processLog(projectName, data, stderr) {
		const lines = data.toString().trim().split("\n");

		let line;
		while((line = lines.shift()) !== undefined) {
			Logger.asProcess(projectName, line, stderr);
		}
	}

	start(projectName) {
		return new Promise((resolve, reject) => {
			if (this.processes[projectName]) return resolve(true);
			if (!this.projectData[projectName]) return reject(new Error(`Project ${projectName} isn't registered.`));

			const projectData = this.projectData[projectName];
			const cwd = Path.join(this.cwd, projectName);
			const process = Process.exec(projectData.startup, {
				cwd: cwd,
			});

			let started = false;
			process.stdout.on('data', data => {
				this.processLog(projectName, data);

				if (!started && data.toString().search(projectData.running) !== -1) {
					started = true;
					this.clearCrashes(projectName);

					return resolve(true);
				}
			});
			process.stderr.on('data', data => this.processLog(projectName, data, true));

			process.on('exit', (code, signal) => {
				if (signal === 'SIGTERM') return; // Shutdown caused by safeShutdown()

				this.addCrash(projectName);
				delete this.processes[projectName];

				if (!started) return reject(new Error('Process exited with code ' + code));
				else {
					if (this.getCrashes(projectName) < 3) {
						Logger.warn(`Process of project ${projectName} crashed with exit code ${code}, starting it again...`);

						this.start(projectName).catch(() => {}); // TODO: Possibly try to install dependencies?
					} else {
						Logger.warn(`Process of project ${projectName} has crashed 3 times in a row without being unable to start, will try again in 5 minutes...`);

						this.clearCrashes(projectName);
						setTimeout(() => {
							this.start(projectName).catch(() => {});
						}, 5 * 60 * 1000);
					}
				}
			});

			setTimeout(() => {
				if (!started) return reject(new Error('Timeout'));
			}, projectData.timeout || 60000);

			this.processes[projectName] = process;
		});
	}
}

module.exports = new ProcessManager();