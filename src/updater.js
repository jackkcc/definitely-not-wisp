const Fs = require('fs');
const Path = require('path');
const {EventEmitter} = require('events');
const request = require('request');
const rimraf = require('rimraf');
const Logger = require('./logger');
const ProcessManager = require('./process-manager');
const PGP = require('./pgp');
const DependencyInstaller = require('./dependency-installer');

class Updater extends EventEmitter {
	constructor() {
		super();

		this.customHandlers = {};
		this.updating = {};
		this.badUpdate = {};
	}

	isUpdating(projectName) {
		return this.updating[projectName] ? true : false;
	}

	addCustomHandler(projectName, handler) {
		this.customHandlers[projectName] = handler;
	}

	resolveFiles(projectName, type) {
		if (type === 'nodejs') {
			return [`${projectName}.wisp`, 'package.json', 'package-lock.json'];
		} else if(type === 'golang') {
			return [projectName, 'version.txt'];
		} else {
			throw new Error('Unhandled software type: ' + type);
		}
	}

	resolveDependencyFolder(type) {
		if (type === 'nodejs') {
			return 'node_modules';
		}
	}

	download(url, output) {
		return new Promise((resolve, reject) => {
			if (Fs.existsSync(output)) Fs.unlinkSync(output);

			const writeStream = Fs.createWriteStream(output, {mode: 0o755});
			let res;

			request.get(url).on('response', _res => {
				res = _res;
			}).pipe(writeStream).on('finish', () => {
				if (res.statusCode !== 200) {
					if (Fs.existsSync(output)) Fs.unlinkSync(output);

					return reject(new Error('Failed downloading file @ ' + url + ' - received status code ' + res.statusCode));
				}

				return resolve(true);
			});
		});
	}

	getSignature(url) {
		return new Promise((resolve, reject) => {
			request.get(url, (err, res, body) => {
				if (err) {
					return reject(err);
				}

				if (res.statusCode !== 200) {
					return reject(new Error('Failed to get signature file, received status code ' + res.statusCode));
				}

				return resolve(body);
			});
		});
	}

	async _update(projectName, {version, type}, shouldRun) {
		if (this.customHandlers[projectName]) {
			await this.customHandlers[projectName](version).catch(err => {
				Logger.warn(`Custom update handler for ${projectName} errored: ${err}`);
			});
		} else {
			const wasRunning = ProcessManager.isRunning(projectName)
			if (wasRunning) {
				Logger.info(`Updating ${projectName} to version ${version}, waiting for existing process to shutdown...`);
				const shutdown = await ProcessManager.safeShutdown(projectName).catch(err => {
					Logger.warn(`Failed shutting down ${projectName}: ${err}`);
				});
				if (!shutdown) {
					Logger.warn(`Couldn't shutdown ${projectName}, avoiding update.`);
					return;
				}
			} else {
				Logger.info(`Updating ${projectName} to version ${version}...`);
			}

			const filesToDownload = this.resolveFiles(projectName, type);
			Logger.debug(`Downloading new version of ${projectName}... [${filesToDownload.join(', ')}]`);

			const mainFile = filesToDownload.shift(); // Assume 1st file in array = signed file
			const releaseUrl = `https://cdn.wisp.gg/releases/${projectName}/${version}`;
			const projectPath = Path.join(process.cwd(), projectName);
			const mainFilePath = Path.join(projectPath, `_tmp_${mainFile}`);
			const logFilePath = Path.join(projectPath, 'update.log');

			if (Fs.existsSync(logFilePath)) Fs.unlinkSync(logFilePath);
			const log = Fs.createWriteStream(logFilePath);
			log.write(`${new Date().toISOString()} - Update ${projectName} (${type}) to version ${version}\n`);

			const recover = () => {
				log.write(`RECOVERY\n`);
				if (Fs.existsSync(mainFilePath)) Fs.unlinkSync(mainFilePath);

				for(const file of filesToDownload) {
					const filePath = Path.join(projectPath, file);
					const oldFilePath = Path.join(projectPath, `_old_${file}`);

					if (Fs.existsSync(filePath)) Fs.unlinkSync(filePath);
					if (Fs.existsSync(oldFilePath)) Fs.renameSync(oldFilePath, filePath);
				}

				log.write(`END`);
				log.close();
			};

			Logger.debug(`Downloading the file ${mainFile}...`);
			log.write(`Downloading ${mainFile} from ${releaseUrl}/${mainFile} to ${mainFilePath}...\n`)
			const mainSuccess = await this.download(`${releaseUrl}/${mainFile}`, mainFilePath).catch(err => {
				Logger.warn(`Failed downloading ${mainFile}: ${err}`);
				log.write(`Failed downloading ${mainFile}: ${err}\n`);

				recover();
			});
			if (!mainSuccess) return;

			Logger.debug('Verifying signature...');
			log.write(`Getting file signature from ${releaseUrl}/${mainFile}.asc...\n`);
			const signature = await this.getSignature(`${releaseUrl}/${mainFile}.asc`).catch(err => {
				Logger.warn(`Failed getting signature for ${mainFile}: ${err}`);
				log.write(`Failed getting file signature for ${mainFile}: ${err}\n`);

				recover();
			});
			if (!signature) return;

			log.write(`File signature for ${mainFile}:\n${signature}\n`);
			log.write(`Verifying signature...\n`);

			const fileSignatureData = await PGP.verifyFile(mainFilePath, signature).catch(err => {
				Logger.warn(`Failed getting signature data for ${mainFile}: ${err}`);
				log.write(`Failed verifying signature for ${mainFile}: ${err}\n`);

				recover();
			});
			if (!fileSignatureData) return;

			if (!fileSignatureData.verified) {
				Logger.warn(`Could not verify the file integrity/authenticity of ${mainFile}, aborting update.`);
				log.write(`Signature is not verified. Key ID: ${fileSignatureData.keyid.toHex().toUpperCase()}\n`);

				recover();
				return;
			}

			Logger.info(`File ${mainFile} is intact and signed by ${fileSignatureData.signedBy}.`);
			log.write(`Signature successfully verified for ${mainFile}, signed by ${fileSignatureData.signedBy}.\n`);

			Logger.info('Downloading project files...');
			log.write('Downloading project files...\n');
			for(const file of filesToDownload) {
				const filePath = Path.join(projectPath, file);
				const oldFilePath = Path.join(projectPath, `_old_${file}`);

				if (Fs.existsSync(oldFilePath)) Fs.unlinkSync(oldFilePath);
				if (Fs.existsSync(filePath)) Fs.renameSync(filePath, oldFilePath);

				Logger.debug(`Downloading the file ${file}...`);
				log.write(`Downloading ${file} from ${releaseUrl}/${file} to ${filePath}...\n`)
				const subFileSuccess = await this.download(`${releaseUrl}/${file}`, filePath).catch(err => {
					Logger.warn(`Failed downloading ${file}: ${err}`);
					log.write(`Failed downloading ${file}: ${err}\n`);

					recover();
				});
				if (!subFileSuccess) return;
			}

			Logger.info('Installing dependencies...');
			log.write(`Downloading dependencies if needed...\n`);
			const dependencySuccess = await DependencyInstaller.install(projectPath, type).catch(err => {
				Logger.warn('Failed installing dependencies: ' + err);
				log.write(`Failed downloading dependencies: ${err}\n`);

				recover();
			});
			if (!dependencySuccess) return;

			const realPath = Path.join(projectPath, mainFile);
			const rollbackPath = Path.join(projectPath, `_rollback_${mainFile}`);
			if (Fs.existsSync(realPath)) Fs.renameSync(realPath, rollbackPath);
			Fs.renameSync(mainFilePath, realPath);

			log.write(`-- UPDATE FINISHED --\n`);

			let started = true;
			if (wasRunning || shouldRun) {
				Logger.info(`Update finished for ${projectName}, new version is ${version}. Proceeding to run the process...`);
				log.write(`Process expected to be running, starting it again...\n`);

				const startedAgain = await ProcessManager.start(projectName).catch(err => {
					Logger.warn(`Failed starting process for ${projectName}: ${err}`);
					log.write(`Starting process for ${projectName} failed: ${err}\n`);
				});
				if (!startedAgain) started = false;

				if (started) {
					Logger.info(`Project ${projectName} is now running.`);
					log.write(`Process started.\n`);
				}
			} else Logger.info(`Update finished for ${projectName}, new version is ${version}.`);

			if (started) {
				log.write(`Cleaning up _old_* files...\n`);
				Logger.debug('Getting rid of _old_* files...');

				if (Fs.existsSync(rollbackPath)) Fs.unlinkSync(rollbackPath);
				for(const file of filesToDownload) {
					const oldFilePath = Path.join(projectPath, `_old_${file}`);

					if (Fs.existsSync(oldFilePath)) Fs.unlinkSync(oldFilePath);
				}

				log.write(`END`);
				log.end();
			} else {
				Logger.info('Starting the updated version failed, rollbacking to previous version...');
				log.write(`Update failed, rollbacking...\n`);

				const rollbackable = Fs.existsSync(rollbackPath);
				if (rollbackable) {
					if (Fs.existsSync(realPath)) Fs.unlinkSync(realPath);

					Fs.renameSync(rollbackPath, realPath);
				}

				for(const file of filesToDownload) {
					const realFilePath = Path.join(projectPath, file);
					const oldFilePath = Path.join(projectPath, `_old_${file}`);

					if (Fs.existsSync(realFilePath)) Fs.unlinkSync(realFilePath);
					if (Fs.existsSync(oldFilePath)) Fs.renameSync(oldFilePath, realFilePath);
				}

				if (rollbackable && Fs.existsSync(realPath)) {
					Logger.debug('Installing dependencies for rollbacked version...');
					log.write(`Installing dependencies for rollbacked version...\n`);

					await DependencyInstaller.install(projectPath, type).catch(err => {
						Logger.warn('Failed installing dependencies for rollbacked version: ' + err);
						log.write(`Failed installing dependencies for rollbacked version: ${err}\n`);
					});

					Logger.info(`Trying to start the process again...`);;
					log.write(`Starting the process again...\n`);
					const startedAnotherAgain = await ProcessManager.start(projectName).catch(err => {
						Logger.warn(`Failed starting rollbacked version of ${projectName}: ${err}`);
						log.write(`Failed starting rollbacked version: ${err}\n`);
					});
					if (!startedAnotherAgain) started = false;
					else started = true;
				} else {
					Logger.debug('Rollbacked version doesn\'t exist.');
					log.write(`Rollbacked version doesn\'t exist.\n`);
				}

				if (!started) {
					Logger.warn('ROLLBACKED VERSION FAILED - DELETING PROJECT IN HOPE OF REINSTALL FIXING THE ISSUE.');
					log.write(`ROLLBACK FAILED - DELETING PROJECT IN HOPE OF REINSTALL FIXING THE ISSUE.\n`);

					if (Fs.existsSync(realPath)) Fs.unlinkSync(realPath);
					for (const file of filesToDownload) {
						const realFilePath = Path.join(projectPath, file);

						if (Fs.existsSync(realFilePath)) Fs.unlinkSync(realFilePath);
					}

					const dependencyFolder = this.resolveDependencyFolder(type);
					if (dependencyFolder) {
						try {
							rimraf.sync(Path.join(projectPath, dependencyFolder));
						} catch(err) {
							Logger.warn(`Failed deleting dependency folder ${dependencyFolder} for ${projectName}: ${err}`);
							log.write(`Failed deleting dependency folder ${dependencyFolder}: ${err}\n`);
						}
					}

					log.write(`END`);
					log.end();

					Logger.info(`Tricking itself into forcing to download good update next version check...`);
					this.emit('update-version', projectName, 'v0.0.0');
				} else {
					Logger.info(`Successfully recovered ${projectName} back to original version.`);
					log.write(`Successfully recovered back to original version, marking ${version} as bad update for 15 minutes.\n`);
					log.write(`END`);
					log.end();

					this.badUpdate[`${projectName}-${version}`] = true;
					setTimeout(() => delete this.badUpdate[`${projectName}-${version}`], 15 * 60 * 1000);
				}
				return false;
			}
		}

		this.emit('update-version', projectName, version);
		return true;
	}

	async update(projectName, projectData, shouldRun) {
		if (this.badUpdate[`${projectName}-${projectData.version}`]) return;
		if (this.updating[projectName]) return;

		this.updating[projectName] = true;
		await this._update(projectName, projectData, shouldRun).catch(err => {
			Logger.warn(`Failed updating ${projectName}: ${err}`);
		});
		delete this.updating[projectName];

		this.emit('updated');
	}

	async bulkUpdate(bulkData, shouldRun) {
		for (const projectName in bulkData) {
			if (this.badUpdate[`${projectName}-${bulkData[projectName].version}`]) {
				delete bulkData[projectName];
				continue;
			}

			if (this.updating[projectName]) {
				delete bulkData[projectName];
				continue;
			}

			this.updating[projectName] = true;
		}

		if (Object.keys(bulkData).length === 0) return;

		for (const projectName in bulkData) {
			await this._update(projectName, bulkData[projectName], shouldRun).catch(err => {
				Logger.warn(`Failed updating ${projectName}: ${err}`);
			});
			delete this.updating[projectName];
		}

		this.emit('updated');
	}
}

module.exports = new Updater();