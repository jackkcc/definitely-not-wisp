const Fs = require('fs');
const Path = require('path');

class VersionResolver {
	get(path, type) {
		if (type === 'nodejs') {
			return require(Path.join(path, 'package.json')).version;
		} else if (type === 'golang') {
			return Fs.readFileSync(Path.join(path, 'version.txt')).toString();
		}

		throw new Error('Unable to resolve version for ' + type);
	}
}

module.exports = new VersionResolver();