class Config {
	get(key, def) {
		return process.env[key] !== undefined ? process.env[key] : def;
	}
}

module.exports = new Config();