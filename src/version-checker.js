const {EventEmitter} = require('events');
const request = require('request');
const Config = require('./config');
const Logger = require('./logger');
const Updater = require('./updater');

class VersionChecker extends EventEmitter {
	constructor() {
		super();

		this.projects = {};
		this.supressedUnsupportedChannel = {};
		this.autorun = false;
	}

	enableAutorun() {
		this.autorun = true;
	}

	setProject(projectName, curVersion, channel) {
		this.projects[projectName] = [curVersion, channel || this.projects[projectName] && this.projects[projectName][1]];
	}

	deleteProject(projectName) {
		delete this.projects[projectName];
	}

	_poll() {
		Logger.debug('Polling...');
		request.get(Config.get('UPDATE_URL', 'https://cdn.wisp.gg/versions.json'), {
			headers: {
				['User-Agent']: 'WISP Bootstrapper/' + require('../package.json').version,
			},
		}, (err, res, body) => {
			if (err || res.statusCode !== 200) {
				Logger.error('Failed checking updates: ' + (err || 'Received status code ' + res.statusCode));
				this.emit('poll', 0);
				return;
			}

			let data;
			try {
				data = JSON.parse(body);
			} catch(err) {}

			if (!data) {
				Logger.error('Failed checking updates: Invalid JSON (' + body.replace(/\n/g, '').substring(0, 500) + ')');
				this.emit('poll', 0);
				return;
			}

			const canUpdate = {};
			for(const projectName in this.projects) {
				if (!data[projectName]) {
					Logger.warn(`The project ${projectName} is non-existant?`);
					continue;
				}
				if (Updater.isUpdating(projectName)) continue;

				const projectData = this.projects[projectName];
				const curVersion = projectData[0];
				const channel = projectData[1] || Config.get('ENVIRONMENT', 'production');
				const latestVersionData = data[projectName].versions[channel];
				if (!latestVersionData) {
					if (!this.supressedUnsupportedChannel[projectName]) Logger.warn(`Unsupported channel ${channel} for project ${projectName}, skipping version checks...`);
					this.supressedUnsupportedChannel[projectName] = true;
					continue;
				}

				const latestVersion = latestVersionData.version;
				Logger.debug(`${projectName} - current: ${curVersion}, latest: ${latestVersion}`);

				if (curVersion !== latestVersion) { // Ignore SemVer so rollbacks could be done
					Logger.info(`Update available for ${projectName}! (current: ${curVersion}, latest: ${latestVersion}; channel: ${channel})`);

					latestVersionData.type = data[projectName].type;
					canUpdate[projectName] = latestVersionData;
				}
			}

			const itemsToUpdate = Object.keys(canUpdate).length;
			this.emit('poll', itemsToUpdate);
			if (canUpdate['bootstrapper']) { // Prioritize bootstrapper updates over all.
				Updater.update('bootstrapper', canUpdate['bootstrapper']);
				return;
			} else {
				if (itemsToUpdate > 0) Updater.bulkUpdate(canUpdate, this.autorun);
			}
		});
	}

	run() {
		if (this.running) throw new Error('VersionChecker is already running.');

		this.running = true;
		this._poll();
		setInterval(this._poll.bind(this), Config.get('POLL_INTERVAL', 60000));
	}
}

module.exports = new VersionChecker();